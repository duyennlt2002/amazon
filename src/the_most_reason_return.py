import pandas as pd
import matplotlib.pyplot as plt

#import data order and remove NaN
data_orders=pd.read_csv('D:\python_dev\project_analytics\orders.csv',encoding= 'unicode_escape')
data_orders=data_orders.dropna()

##import data return and remove NaN
data_returns=pd.read_csv('D:\python_dev\project_analytics\data_returns.csv',encoding='unicode_escape')
data_returns=data_returns.dropna()

#data orders left join with data returns
#data_all=data_orders.merge(data_returns,on='order_id',how='left')
data_all=data_orders.join(data_returns,lsuffix="_left", rsuffix="_right")


    
def the_most_reason_return ():
    #replace $ -> ''
    data_all['sales']=data_all['sales'].str.replace(r"$","").dropna()
    data_all['profit']=data_all[' profit _left'].str.replace(r"$","").dropna()

    data_all['sales']=data_all['sales'].str.strip().str.replace(',', '')
    data_all['profit']=data_all['profit'].str.strip().str.replace(',', '').str.replace('-', '')
    #cast string -> float
    data_all['sales']=pd.to_numeric(data_all['sales'])
    data_all['profit']=pd.to_numeric(data_all['profit'], errors='coerce')
    data_all['real_quantity']=data_all['quantity'].fillna(0)-data_all['return_quantity'].fillna(0)
    
    # revenue group by segment
    #revenue=data_all.groupby(['salesperson'])['sales'].sum()
    #segment=data_all.groupby(['segment'])['sales'].sum()
    # profit group by segment
    #profits=data_all.groupby(['salesperson'])['profit'].sum()

    quantity=data_all.groupby(['return_reason_right'])['return_quantity'].sum().plot(kind='pie', y='points', autopct='%1.0f%%',
                                colors = ['red', 'pink', 'steelblue','gray'],
                                title='Percent Reason Return')
    plt.show()

the_most_reason_return ()