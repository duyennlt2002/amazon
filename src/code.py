#importing pandas as pd
import pandas as pd
  
# Read and store content
# of an excel file 
lookup = pd.read_csv ("MarginLookUp.csv")
orders = pd.read_csv ("order.csv")
returns = pd.read_csv ("returns.csv")

# Write the dataframe object

#  into a dataframe object

df_lookup = pd.DataFrame(lookup)
df_orders = pd.DataFrame(orders)
df_returns = pd.DataFrame(returns)

# show the dataframe
print(df_lookup.head())
print(df_orders.head())
print(df_returns.head())
