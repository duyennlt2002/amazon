import pandas as pd
import matplotlib.pyplot as plt
from itertools  import combinations
from collections import Counter



def products():
    #import data order and remove NaN
    data_orders=pd.read_csv('D:\python_dev\project_analytics\orders.csv',encoding= 'unicode_escape')
    data_orders=data_orders.dropna()

    ##import data return and remove NaN
    data_returns=pd.read_csv('D:\python_dev\project_analytics\data_returns.csv',encoding='unicode_escape')
    data_returns=data_returns.dropna()

    #data orders left join with data returns
    #data_all=data_orders.merge(data_returns,on='order_id',how='left')
    data_all=data_orders.join(data_returns,lsuffix="_left", rsuffix="_right")
    
    data_all['Product_Sold'] = data_all.groupby('order_id_left')['product_name'].transform(lambda x: ','.join(x))
    data_all=data_all[['order_id_left','Product_Sold']].drop_duplicates()
    count=Counter()
    for row in data_all['Product_Sold']:
        row_list=row.split(',')
        count.update(Counter(combinations(row_list,2)))
        #count.update(Counter(combinations(row_list,3)))
    #print(count)
    for key,value in count.most_common(10):
        print(key, value)

products()