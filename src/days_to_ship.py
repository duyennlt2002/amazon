import pandas as pd
import matplotlib.pyplot as plt
from itertools  import combinations
from collections import Counter

#import data order and remove NaN
data_orders=pd.read_csv('D:\python_dev\project_analytics\orders.csv',encoding= 'unicode_escape')
data_orders=data_orders.dropna()

##import data return and remove NaN
data_returns=pd.read_csv('D:\python_dev\project_analytics\data_returns.csv',encoding='unicode_escape')
data_returns=data_returns.dropna()

#data orders left join with data returns
#data_all=data_orders.merge(data_returns,on='order_id',how='left')
data_all=data_orders.join(data_returns,lsuffix="_left", rsuffix="_right")

def shipping_days():
    #replace $ -> ''
    data_all['sales']=data_all['sales'].str.replace(r"$","").dropna()
    data_all['profit']=data_all[' profit _left'].str.replace(r"$","").dropna()
    

    data_all['sales']=data_all['sales'].str.strip().str.replace(',', '')
    data_all['profit']=data_all['profit'].str.strip().str.replace(',', '').str.replace('-', '')
    #cast string -> float
    data_all['sales']=pd.to_numeric(data_all['sales'])
    data_all['profit']=pd.to_numeric(data_all['profit'], errors='coerce')
    data_all['real_quantity']=data_all['quantity'].fillna(0)-data_all['return_quantity'].fillna(0)

    revenue=data_all.groupby(['product_name'])['sales'].sum()
    #segment=data_all.groupby(['segment'])['sales'].sum()
     # profit group by segment
    profits=data_all.groupby(['product_name'])['profit'].sum()
    quantity=data_all.groupby(['product_name'])['real_quantity'].sum()
    top_10=quantity.nlargest(n=10)
    day_to_ship=data_all.groupby(['days_to_ship'])['order_id_left'].nunique()

    

    segments = [segment for segment,  segments in day_to_ship.items()]
    plt.bar(segments , day_to_ship, color ='orange',width = 0.8)
    plt.xticks(rotation=90)
    plt.xlabel("Days")
    plt.ylabel("Numbers of Orrder")
   
    plt.show()


shipping_days()