import pandas as pd
import matplotlib.pyplot as plt

#import data order and remove NaN
data_orders=pd.read_csv('D:\python_dev\project_analytics\orders.csv',encoding= 'unicode_escape')
data_orders=data_orders.dropna()

##import data return and remove NaN
data_returns=pd.read_csv('D:\python_dev\project_analytics\data_returns.csv',encoding='unicode_escape')
data_returns=data_returns.dropna()

#data orders left join with data returns
#data_all=data_orders.merge(data_returns,on='order_id',how='left')
data_all=data_orders.join(data_returns,lsuffix="_left", rsuffix="_right")#



    
def q17 ():
    #replace $ -> ''
    # data_all['sales']=data_all['sales'].str.replace(r"$","").dropna()
    # data_all['profit']=data_all[' profit _left'].str.replace(r"$","").dropna()

    # data_all['sales']=data_all['sales'].str.strip().str.replace(',', '')
    # data_all['profit']=data_all['profit'].str.strip().str.replace(',', '').str.replace('-', '')
    # #cast string -> float
    # data_all['sales']=pd.to_numeric(data_all['sales'])
    # data_all['profit']=pd.to_numeric(data_all['profit'], errors='coerce')

    # revenue=data_all.groupby(['order_month'])['sales'].sum()
    #segment=data_all.groupby(['segment'])['sales'].sum()
    # profit group by segment
    result=data_all.groupby(['days_to_ship'])['return_quantity'].sum()
    # print(segment)
    segments = [segment for segment,  segments in result.items()]
    plt.bar(segments , result, color ='orange',width = 0.8)
    plt.xticks(rotation=90)
    plt.xlabel("Days")
    plt.ylabel("Numbers of Orrder")
   
    plt.show()

q17()