import pandas as pd
import matplotlib.pyplot as plt
import os
from definitions import ROOT_DIR

order_path=os.path.join(ROOT_DIR, 'data', 'orders.csv')
return_path=os.path.join(ROOT_DIR, 'data', 'returns.csv')

#import data order and remove NaN
data_orders=pd.read_csv(order_path,encoding= 'unicode_escape')
data_orders=data_orders.dropna()

##import data return and remove NaN
data_returns=pd.read_csv(return_path,encoding= 'unicode_escape')
data_returns=data_returns.dropna()

#data orders left join with data returns
#data_all=data_orders.merge(data_returns,on='order_id',how='left')
data_all=data_orders.join(data_returns,lsuffix="_left", rsuffix="_right")
sort_month=['January','February','March','April','May','June','July','August','September','October','November','December']

data_all['order_month']=pd.CategoricalIndex(data_all['order_month'],categories=sort_month,ordered=True)
data_all=data_all.sort_index().reset_index(drop=True)
    
def best_moth_sales ():
    #replace $ -> ''
    data_all['sales']=data_all['sales']
    data_all['profit']=data_all['profit']

    data_all['sales']=data_all['sales']
    data_all['profit']=data_all['profit']
    #cast string -> float
    data_all['sales']=pd.to_numeric(data_all['sales'])
    data_all['profit']=pd.to_numeric(data_all['profit'], errors='coerce')

    revenue=data_all.groupby(['order_month'])['sales'].sum()
    #segment=data_all.groupby(['segment'])['sales'].sum()
     # profit group by segment
    profits=data_all.groupby(['order_month'])['profit'].sum()
    # print(segment)
    segments = [segment for segment,  segments in revenue.items()]
    fig,ax1=plt.subplots()

    ax2=ax1.twinx()
    ax1.bar(segments,revenue,color ='orange',width =0.5)
    ax2.plot(segments,profits)
    ax1.set_xlabel('Month')
    ax1.set_ylabel('Revenue')
    ax2.set_label('Profit')
    ax1.set_xticklabels(segments,rotation=90)
    plt.show()

best_moth_sales()