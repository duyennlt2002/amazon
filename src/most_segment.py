import pandas as pd
import matplotlib.pyplot as plt

#import data order and remove NaN
data_orders=pd.read_csv('D:\python_dev\project_analytics\orders.csv',encoding= 'unicode_escape')
data_orders=data_orders.dropna()

##import data return and remove NaN
data_returns=pd.read_csv('D:\python_dev\project_analytics\data_returns.csv',encoding='unicode_escape')
data_returns=data_returns.dropna()

#data orders left join with data returns
#data_all=data_orders.merge(data_returns,on='order_id',how='left')
data_all=data_orders.join(data_returns,lsuffix="_left", rsuffix="_right")

def segment_spends_the_most ():
    #replace $ -> ''
    data_all['sales']=data_all['sales'].str.replace(r"$","").dropna()
    data_all['profit']=data_all[' profit _left'].str.replace(r"$","").dropna()

    data_all['sales']=data_all['sales'].str.strip().str.replace(',', '')
    data_all['profit']=data_all['profit'].str.strip().str.replace(',', '').str.replace('-', '')
    #cast string -> float
    data_all['sales']=pd.to_numeric(data_all['sales'])
    data_all['profit']=pd.to_numeric(data_all['profit'], errors='coerce')
    
    # revenue group by segment
    revenue=data_all.groupby(['segment'])['sales'].sum()
    #segment=data_all.groupby(['segment'])['sales'].sum()
     # profit group by segment
    profits=data_all.groupby(['segment'])['profit'].sum()
    # print(segment)
    segments = [segment for segment,  segments in revenue.items()]
    fig,ax1=plt.subplots()

    ax2=ax1.twinx()
    ax1.bar(segments,revenue,color ='orange',width =0.5)
    ax2.plot(segments,profits)
    ax1.set_xlabel('Segment')
    ax1.set_ylabel('Revenue')
    ax2.set_label('Profit')
    ax1.set_xticklabels(segments,rotation=0)
    plt.show()

segment_spends_the_most ()